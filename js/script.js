// Working with cookie
var cookie = {
  get: function(name) {
    var matches = document.cookie.match(new RegExp(
      "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
  },
  set: function(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
      var d = new Date();
      d.setTime(d.getTime() + expires * 1000);
      expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
      options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
      updatedCookie += "; " + propName;
      var propValue = options[propName];
      if (propValue !== true) {
        updatedCookie += "=" + propValue;
      }
    }

    document.cookie = updatedCookie;
  },
  delete: function(name) {
    this.set(name, "", {
      expires: -1,
      path: "/"
    });
  }

}



// JavaScript main code
$( document ).on('ready', function () {
    if (!cookie.get("unsuggestMobile")){
        $("div.warning i").on("click", function() {
            cookie.set("unsuggestMobile", 1 , {path: "/"});
            $("div.warning").remove();
        })
    } else {
      $('div.warning').remove();
    }
    var slideout = new Slideout({
      'panel': document.getElementById('main'),
      'menu': document.getElementById('navbar'),
      'padding': 256,
      'tolerance': 70,
      'duration': 500
    });
    // Toggle button
    document.querySelector('.hamburger').addEventListener('click', function() {
      slideout.toggle();
    });
    $(window).resize(function() {
      if ($(window).width() > 570) {
        slideout.close();
      }
    });
});